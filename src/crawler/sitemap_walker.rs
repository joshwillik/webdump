use std::io::Cursor;
use sitemap::reader::{SiteMapReader, SiteMapEntity};
use sitemap::structs::{Location, UrlEntry};
use reqwest;

pub fn walk(url: &str) -> Result<(), Box<dyn std::error::Error>> {
    let resp = reqwest::blocking::get(url)?.text()?;
    let reader = SiteMapReader::new(Cursor::new(resp));
    for entity in reader {
        match entity {
            SiteMapEntity::Url( UrlEntry {loc: Location::Url(url), ..}) => {
                println!("url: {}", url);
            }
            SiteMapEntity::SiteMap(_) => {
                println!("TODO josh: need to process subsitemap");
            }
            _ => {
                println!("No idea how to read this");
            }
        }
    }
    Ok(())
}