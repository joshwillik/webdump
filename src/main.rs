// Command line application that supports the following commands
// webdump discover --host <host>
// webdump crawl --host <host>
// webdump index --host <host>
mod url_store;
mod crawler;

use std::error::Error;
use url_store::SqliteUrlStore;
use crawler::sitemap_walker;

enum Command {
    Discover { host: String },
    Crawl { host: String },
    Index { host: String },
}

// TODO josh: replace this with a more generic arg parser like docopt
fn parse_command() -> Command {
    let argv = std::env::args();
    let mut argv = argv.skip(1);
    let command = argv.next().unwrap_or_else(|| {
        eprintln!("Missing command");
        std::process::exit(1);
    });
    match command.as_str() {
        "discover" => {
            let host = argv.next().unwrap_or_else(|| {
                eprintln!("Missing host");
                std::process::exit(1);
            });
            Command::Discover { host }
        }
        "crawl" => {
            let host = argv.next().unwrap_or_else(|| {
                eprintln!("Missing host");
                std::process::exit(1);
            });
            Command::Crawl { host }
        }
        "index" => {
            let host = argv.next().unwrap_or_else(|| {
                eprintln!("Missing host");
                std::process::exit(1);
            });
            Command::Index { host }
        }
        _ => {
            eprintln!("Unknown command {}", command);
            std::process::exit(1);
        }
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    match parse_command() {
        Command::Discover { host } => {
            let db_path = format!("data/{}.sqlite", host.replace(".", "_"));
            let store = SqliteUrlStore::ensure(&db_path);
            sitemap_walker::walk(format!("https://{}/sitemap.xml", host).as_str())?;
        }
        Command::Crawl { host } => {
            println!("Crawling {}", host);
        }
        Command::Index { host } => {
            println!("Indexing {}", host);
        }
    }
    Ok(())
}
