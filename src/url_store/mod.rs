use sqlite;
use url::Url;

#[allow(dead_code)]
pub struct SqliteUrlStore {
    pub db_path: String,
    conn: sqlite::Connection,
}

const ENSURE_URLS_TABLE_SQL: &'static str = "
    CREATE TABLE IF NOT EXISTS urls (
        id INTEGER PRIMARY KEY,
        url TEXT NOT NULL UNIQUE,
        host TEXT,
        discovered_at TEXT NOT NULL DEFAULT CURRENT_TIMESTAMP
    );
    create index if not exists urls_host_idx on urls (host);
";

#[allow(dead_code)]
impl SqliteUrlStore {
    pub fn ensure(db_path: &String) -> Self {
        let conn = sqlite::open(db_path).unwrap();
        conn.execute(ENSURE_URLS_TABLE_SQL).unwrap();
        SqliteUrlStore {
            db_path: db_path.clone(),
            conn,
        }
    }
    fn add_url(&mut self, url: &String) -> Result<(), String> {
        let host = Url::parse(url).map_err(|e| e.to_string())?;
        let host = host.host_str().unwrap();
        let mut stmt = self.conn.prepare("INSERT INTO urls (url, host) VALUES (?1, ?2)").unwrap();
        stmt.bind((1, url.as_str())).unwrap();
        stmt.bind((2, host.clone())).unwrap();
        stmt.next().unwrap();
        Ok(())
    }
}